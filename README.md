# Test Python Servier

## Installation

Bibliothèques nécessaires:
- **json**
- **csv**

Les fichiers **pubmed.csv**, **drugs.csv** et **clinicla_trials.csv** doivent être dans le même dossier que les scripts python

## Lancement des scripts

Création du json
```
$ python pipeline.py
```
Traitement ad-hoc
```
$ python traitement.py
```
## Pour aller plus loin

Afin de traiter de grande bases de données, plusieurs améliorations peuvent être faites :
- Création d'un script qui met à jour le json au lieu de l'écrire à partir de zéro
- Stocker les données dans une database MySQL, Hadoop ou MongoDB
- Partager le travail entre différents processus qui ecrivent et lisent dans une même database
- Création d'un script qui met a jour la database a chaque création d'article

## 

## Requêtes SQL

Première partie du Test
```
SELECT date, SUM(prod_qty) AS ventes FROM TRANSACTION
```
Seconde partie du Test
```
SELECT  client_id,
	SUM(CASE WHEN prop.id='MEUBLE' THEN 'prop_qty' ELSE 0 END) AS vente_meuble,
	SUM(CASE WHEN prop.id='DECO' THEN 'prop_qty' ELSE 0 END) AS vente_deco,
FROM TRANSACTION
JOIN PRODUCT_NOMENCLATURE 
ON TRANSACTION.prop.id = PRODUCT_NOMENCLATURE.product_id
GROUP BY client_id
```


