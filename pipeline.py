import json, csv


def dictionary_from_csv(filename, key_column = 'id'):

    """Dictionnaire depuis un csv.

    Cette fonction cree un dictionnaire python
    a partir d'un csv

    Parameters
    ----------
    filename : str
        path vers le csv
    key_column : str
        La cle qui sera la clef du dictionnaire

    Returns
    -------
    dictionnaire
        Un dictionnaire dans lequel les noms 
        des colonnes sont les cles
        et les valeurs sont celles des colonnes du csv
    """

    data = {}
    with open(filename) as csvfile:
        csvReader = csv.DictReader(csvfile)
        for csvRow in csvReader:
            key = csvRow[key_column]
            if key =='': 
                key = 'generated_id_'+''.join([val[:3] for val in csvRow.values() if len(val)>3])
            elif '\t' not in key:
                data[key] = csvRow
    return data



def str_in_str(word,sentence):

    """Presence d'une string dans une string.

    Cette fonction renvoie True si un mot est present dans une phrase
    Elle ne prend pas en compte les majuscules/minuscules

    Parameters
    ----------
    word : str
        Le mot a chercher
    sentence : str
        La phrase dans laquelle on cherche le mot

    Returns
    -------
    boolean
        TRUE sir word est dans sentence
        FALSE sinon
    """

    return str.lower(word) in str.lower(sentence)




def init():

    """Initialisation des dictionnaires

    Cette fonction initialise les dictionnaires

    Parameters
    ----------

    Returns
    -------
    
    """

    # Les cles 'pubmed', 'journal', 'clinical_trial' contiendront
    # les articles qui mentionnent la drug en question
    for drug in drugs.keys():
        drugs[drug]['pubmed']= []
        drugs[drug]['journal']= []
        drugs[drug]['clinical_trial'] = []

    # On ajoute aux publications le type de publication
    for pubmed in pubmeds.keys():
        pubmeds[pubmed]['type_paper'] = 'pubmed'
    for clinical_trial in clinical_trials.keys():
        clinical_trials[clinical_trial]['type_paper'] = 'clinical_trial'
    return

# Cette fonction ajoute a une drug une publication qui lui fait reference
def add_quotation(drug, publication):
    
    """Ajout d'une citation

    Cette fonction ajoute une citation d'une drug
    dans une publication

    Parameters
    ----------
    drug : str
        la drug citee
    publication : str
        Le titre de la publication dans lequel il est cite

    Returns
    -------

    """
    
    drugs[drug][publication['type_paper']]\
        .append({'date':publication['date'], 'id':publication['id']})
    drugs[drug]['journal']\
        .append({'date' : publication['date'],'journal' :publication['journal'],\
                 'type_paper':publication['type_paper'],'id': publication['id'] })
    return

def main():
    # Importation des csv 
    global drugs, pubmeds, clinical_trials
    drugs = dictionary_from_csv('drugs.csv','drug')
    pubmeds = dictionary_from_csv('pubmed.csv')
    clinical_trials = dictionary_from_csv('clinical_trials.csv')
    

    # Initialisation des dictionnaire
    init()   

    # Pour chaque drug, on ajoute les citations a condition 
    # que le nom de la drug soit dans le titre
    for drug in drugs.keys():

        # boucle sur les pubmeds
        for pubmed in pubmeds.keys():
            if str_in_str(drug,pubmeds[pubmed]['title']):
                add_quotation(drug, pubmeds[pubmed])

        # boucle sur les clinical trials
        for clinical_trial in clinical_trials.keys():
            if str_in_str(drug,clinical_trials[clinical_trial]['scientific_title']):
                add_quotation(drug, clinical_trials[clinical_trial])

    # Creation du dictionnaire final
    graph = {'drugs':drugs, 'pubmeds': pubmeds, 'clinical_trials':clinical_trials}

    # Sauvegarde du dictionnaire dans graph.json
    with open('graph.json', 'w', encoding='utf-8') as f:
        json.dump(graph, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    main()
