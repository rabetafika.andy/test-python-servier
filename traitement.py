import json

# Ce fichier python affiche le nom du journal
# qui cite le plus de drugs differentes

# Importation du graph
with open('graph.json',) as f:
    data = json.load(f)

# journals sera un dictionaire sous la forme suivante:
# {'nom_de_journal_1':['drug_1','drug_2'],
#   'nom_de_journal_2':['drug_3'],..}
journals = {}

# Iteration sur les drugs
for drug in data['drugs'].keys():
    # Iteration sur les journaux
    for journal in data['drugs'][drug]['journal']:
        journal_name = journal['journal']

        if journal_name not in journals.keys():
            journals[journal_name] = []
        if drug not in journals[journal_name]:
            journals[journal_name].append(drug)

# Maintenant, on peut chercher le journal qui cite
#   le plus de drugs differentes
max = 0
for journal in journals.keys():
    if len(journals[journal]) > max:
        res = journal
        max = len(journals[journal])

print('Le journal qui cite le plus de drugs est ',res,
      ' et il cite ', max, ' drugs')
